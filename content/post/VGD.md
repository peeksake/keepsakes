+++
author = "Jesse Peek"
title =  "OMSCS - VGD"
date =   "2022-07-28" 
description = "Video Game Design Retrospective"
tags = [
    "VGD", "OMSCS"
]
+++

A look into the content of the OMSCS VGD class.
<!--more-->

## Introduction

Going along with my my IIS class I took Video Game Design, and having dabbled with some Unity in the past and being a gamer, I was quite interested to see where this class would take me. I put out an early advirtisement for those looking to make a puzzle game, kind of already having an idea of what I wanted to do, this worked out well and there ended up being a great amount of collaboration very early on.

## Assignments

There are some great warmups that are pretty well guided to get you going in the world of unity, I found these to be really useful, as they really help familarize you with the Unity Editor, which is by far the biggest learning curve.

But the largest portion of this class is the group project which is a semester long very open game project, with a group of 4 to 5 people. Very early on I got into a group of guys. I had a lot of fun but it was difficult to find place in the project at time, also having the other class to do. I worked on the controller for the character, and I think I could have done a lot better with it, it was very unsophisticated and simply didn't adhered to anything but the flattest of ground. Animations are a painstaking process to build and it's amazing they are as fluid today as they are in modern games.

The team though did a great job, you can see the trailer for our game here.
[Demo Video at 20 minutes](https://mediaspace.gatech.edu/media/CS6457+Video+Game+Design+%28OMSCS%29+-+Spring+2022+-+Final+Game+Project+Trailers+-+Part+I/1_vj66d78m)
## Lectures

The lectures start out pretty general about the types of video games and how they work and then go into a set of Unity specific lessons, which are applicable to the course.

## Takeaways

A deepfound new respect for making games.
Some good knowledge on Unity.

## Rating

Definetly a solid class that delves into the nuts and bolts of making games with Unity, which is a great starting place for any developer looking to get into games.

## Recommended Album
[Palace - So Long Forever](https://www.youtube.com/watch?v=noez-N3GwU8&list=PLynTMOd2BVNMkvJGwp5i01p1hdfz6KUKj)