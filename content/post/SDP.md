+++
author = "Jesse Peek"
title =  "OMSCS - SDP"
date =   "2022-07-27" 
description = "Software Development Retrospective"
tags = [
    "SDP", "OMSCS"
]
+++

A look into the content of the OMSCS SDP class.
<!--more-->

## Introduction

I wanted another simple class to take during the Summer as I was moving into my new home something I had been planning for quite some time. Thankfully but regretfully I got exactly what I asked for, a pretty dull class that I didn't get much out of. 

This class was likely intended for people who are not currently Software Engineers, to mock what a developer work life might be like, though the most tedious way possible. 

## Assignments

I was assigned a team of others for a group project. They were entirely pleasant and I felt like the odd one out for my inconsistency with the project. The task was a simple android project, I have a [published Android App](https://play.google.com/store/apps/details?id=com.parkingoracle.parkingoracle&gl=US) so this was like refreshing old memory. There were other java based tasks that I found to be quite tiresome but not too painful or anything to report on.

## Lectures

There are some good tidbits about testing in the lecture, I wish more engineers took pride in their test coverage, but TDD is a pipedrem, if you actually make tests first before you develop, you're a madman and deserve to be jailed.

## Takeaways

You get what you put in, and classes are not created equally.

Testing, testing, testing.

## Rating

You should not take this class if you are looking for interest or engagement.

## Recommended Album
[Marvin Gaye - What's Going On](https://www.youtube.com/watch?v=H-kA3UtBj4M)