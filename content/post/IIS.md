+++
author = "Jesse Peek"
title =  "OMSCS - IIS"
date =   "2022-07-28" 
description = "Intro Information Security Retrospective"
tags = [
    "IIS", "OMSCS"
]
+++

A look into the content of the OMSCS IIS class.
<!--more-->

## Introduction

This was actually the first class I had taken at GTECH OMSCS but due to inconsistencies with the first project I dropped about half way in. So this go round I decided to pair it with another class as the first two projects I knew weren't going to be difficult at all.

## Assignments

I quite liked the assignments in this class a stack overflow attack is a great opener, and if you've never written anything low level, the ramp up on C knowledge will be quite substantial on this. 

The second project, malware clustering was not very interesting, I'm pretty adverse to data science work if ML stuff ticks your proverbial boxes you might like this more.

The web security project is pretty interesting, gave me some insight to all the CORS errors I've had in the past, it truly made me thankful that the modern day browser does so much heavy lifting when it comes to security in the browser.

## Lectures

I honestly don't remember anything remarkable about these lectures, there's some interesting stuff about network security that 

## Takeaways

The modern web browser is a modern marvel.

## Rating

Good introductury or filler course to go along side another easy to mediumish course.

## Recommended Album
[C418 - Minecraft Volume Alpha](https://www.youtube.com/watch?v=m0apGY2GmFU)