+++
author = "Jesse Peek"
title =  "OMSCS - CN"
date =   "2022-07-26" 
description = "Computer Networks Retrospective"
tags = [
    "GIOS", "OMSCS"
]
+++

A look into the content of the OMSCS CN class.
<!--more-->

## Prelude

It's been a while since I updated this blog, things have gotten a little busy round my parts but it seems like I finally have sometime to update my progress on what I've done so far for my master's in computer science, and I'm feeling a bit ambitious, so I'm looking to update all 4 classes I've taken since Spring 2021.

## Introduction
Hot off the intrigue of GIOS, I was looking forward to more content, but I wanted a bit of a hiatus from the stressful nights that GIOS had me endure. I also took an interest in socket programming thanks to GIOS. Naturally since I had such a good look into what internet traffic was at a very low level, it seemed natural to go wider and look at the whole stack of computer networks.

The very beginning of this class is honestly one of my favorite bits of it. The history of the internet is a story I'm waiting to be told well by some hollywood endeavor. ARPANET, RENO, and some of the brightest most forward thinking minds of the time all contributing to what would honestly not pick up for a good thirty years. It was a time when computers were not yet defined so everything was a challenge. 

Fast-forwarding to established internet-ville the broad stroke of internet architecture can be easily defined by the OSI 7 layer model.
This really becomes the key to understanding how traffic goes from your computer to some server somewhere and then gets translated to that JSON data it'll inevitably become.

## Lectures

The course then guides through the most import layers that people generally overlook: transport, network, and data link. Explaining in detail what's in a TCP packet, how a session is started and securely made. This sauce is key to the brilliant effortless internet we have today.

After going through the majority of the OSI layer there's lot to do with routing, which is interesting in theory but I've never found myself in a position where any of that stuff is particularly applicable. When the scale starts to get larger though and we're talking about ISPs/CDNs, that's when my interest starting to pick up again.

## Assignments

For all the assignments in this class you use python to simulate some network level. In that way it mimics the class material, starting out at layer two where you are making a graphing algorithm from the perspective of a single node. Then the perspective of a router where you are defining the most optimal routes from nodes. Having been super familiar with python and graphing algorithms I found these to be quite fun. 

After this unfortunately there was a jupyter notebook data analysis project that was new that semester that I did not enjoy.


## Takeaways

If you understand TCP the whole internet very top to bottom is able to be deciphered.
Wireshark is a really cool.

## Rating

If you like the internet and you wanna know how it works this is definetly the class for you.

## Recommended Album
[MACROBLANK - 絶望に負けた](https://www.youtube.com/watch?v=xI4u3FSBJSk)