---
title: "well hello there"

description: "introduction"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

# Well Hello There
If you have found yourself here then I hope you enjoy your stay, get some tea, or coffee and enjoy the ride. Tangents are welcome here, so strap in your opposite and adjacent sides.
